﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour
{
    public Text victoryText;
    public Image background;

    [SerializeField] private GameSettings gameSettings;

    public void Setup(GameLogic.GameState gameState)
    {
        gameObject.SetActive(true);
        string text = "DRAW =(";
        if (gameState == GameLogic.GameState.FinishedVictoryCross)
        {
            background.color = gameSettings.crossBackgroundColor;
            text = "CROSS WON!";
        } 
        else if (gameState == GameLogic.GameState.FinishedVictoryNought)
        {
            background.color = gameSettings.noughBacgroundColor;
            text = "NOUGHT WON!";
        }
        else
        {
            background.color = Color.gray;
        }
        victoryText.text = text;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
