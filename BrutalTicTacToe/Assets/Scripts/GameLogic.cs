﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour
{
    public event Action TurnCompleted = () => { };
        
    public struct CellIndex
    {
        public int row;
        public int col;

        public CellIndex(int row, int col)
        {
            this.row = row;
            this.col = col;
        }

        public override int GetHashCode() =>
            row.GetHashCode() ^ col.GetHashCode();
        public override bool Equals(object obj) =>
            obj is CellIndex cellIndex && this == cellIndex;

        public static bool operator ==(CellIndex l, CellIndex r)
        {
            return l.row == r.row && l.col == r.col;
        }
        public static bool operator !=(CellIndex l, CellIndex r)
        {
            return !(l == r);
        }
    };

    public enum Opponent
    {
        None = 0,
        Cross = 1,
        Nought = 2
    };

    public enum GameState
    {
        InProgress = 0,
        FinishedVictoryCross = 1,
        FinishedVictoryNought = 2,
        FinishedDraw = 3
    };

    const int FIELD_COLS_COUNT = 3;
    const int FIELD_ROWS_COUNT = 3;
    const int CELLS_TO_WIN = 3;
    public static readonly CellIndex INVALID_CELL = new CellIndex(-1, -1);

    public GameOverScript gameOverScript;
    public GameObject bombPrefab;
    public Opponent currentPlayerType = Opponent.Cross;
    public GameState gameState = GameState.InProgress;
    public CellIndex currentClickedCell = INVALID_CELL;
    Opponent[,] cellValues = new Opponent[FIELD_ROWS_COUNT, FIELD_COLS_COUNT];

    void Start()
    {
    }

    public void generateBomb(Vector3 worldPosition)
    {
        Instantiate(bombPrefab, new Vector3(worldPosition.x, 9.0f, worldPosition.z), Quaternion.Euler(0, 0, 180));
    }

    bool allCellsFilled()
    {
        for (var i = 0; i < FIELD_ROWS_COUNT; ++i)
        {
            for (var j = 0; j < FIELD_COLS_COUNT; ++j)
            {
                if (cellValues[i, j] == Opponent.None)
                {
                    return false;
                }
            }
        }
        return true;
    }

    bool checkVerticalVictory(CellIndex index)
    {
        int nearSameCells = 0;
        int col = index.col;

        // go up:
        for (int row = index.row; row >= 0; --row)
        {
            if (cellValues[row, col] == currentPlayerType)
            {
                ++nearSameCells;
                if (nearSameCells == CELLS_TO_WIN)
                {
                    return true;
                }
            }
            else
            {
                break;
            }
        }

        // go down:
        for (int row = index.row + 1; row < FIELD_ROWS_COUNT; ++row)
        {
            if (cellValues[row, col] == currentPlayerType)
            {
                ++nearSameCells;
                if (nearSameCells == CELLS_TO_WIN)
                {
                    return true;
                }
            }
            else
            {
                break;
            }
        }
        return false;
    }

    bool checkHorizontalVictory(CellIndex index)
    {
        int nearSameCells = 0;
        int row = index.row;

        // go left:
        for (int col = index.col; col >= 0; --col)
        {
            if (cellValues[row, col] == currentPlayerType)
            {
                ++nearSameCells;
                if (nearSameCells == CELLS_TO_WIN)
                {
                    return true;
                }
            }
            else
            {
                break;
            }
        }

        // go right:
        for (int col = index.col + 1; col < FIELD_COLS_COUNT; ++col)
        {
            if (cellValues[row, col] == currentPlayerType)
            {
                ++nearSameCells;
                if (nearSameCells == CELLS_TO_WIN)
                {
                    return true;
                }
            }
            else
            {
                break;
            }
        }
        return false;
    }

    bool checkDiagonalLeftVictory(CellIndex index)
    {
        int nearSameCells = 0;

        // go left up:
        int row = index.row;
        for (int col = index.col; col >= 0 && row >= 0; --col, --row)
        {
            if (cellValues[row, col] == currentPlayerType)
            {
                ++nearSameCells;
                if (nearSameCells == CELLS_TO_WIN)
                {
                    return true;
                }
            }
            else
            {
                break;
            }
        }

        // go right down:
        row = index.row + 1;
        for (int col = index.col + 1; col < FIELD_COLS_COUNT && row < FIELD_ROWS_COUNT; ++col, ++row)
        {
            if (cellValues[row, col] == currentPlayerType)
            {
                ++nearSameCells;
                if (nearSameCells == CELLS_TO_WIN)
                {
                    return true;
                }
            }
            else
            {
                break;
            }
        }
        return false;
    }

    bool checkDiagonalRightVictory(CellIndex index)
    {
        int nearSameCells = 0;

        // go right up:
        int row = index.row;
        for (int col = index.col; col < FIELD_COLS_COUNT && row >= 0; ++col, --row)
        {
            if (cellValues[row, col] == currentPlayerType)
            {
                ++nearSameCells;
                if (nearSameCells == CELLS_TO_WIN)
                {
                    return true;
                }
            }
            else
            {
                break;
            }
        }

        // go left down:
        row = index.row + 1;
        for (int col = index.col - 1; col >= 0 && row < FIELD_ROWS_COUNT; --col, ++row)
        {
            if (cellValues[row, col] == currentPlayerType)
            {
                ++nearSameCells;
                if (nearSameCells == CELLS_TO_WIN)
                {
                    return true;
                }
            }
            else
            {
                break;
            }
        }
        return false;
    }

    void gameEnd()
    {
        gameOverScript.Setup(gameState);
    }

    public void processTurn(CellIndex index)
    {
        if (cellValues[index.row, index.col] == Opponent.None)
        {
            cellValues[index.row, index.col] = currentPlayerType;

            // check victory
            bool isVictory = checkHorizontalVictory(index);
            isVictory |= checkVerticalVictory(index);
            isVictory |= checkDiagonalLeftVictory(index);
            isVictory |= checkDiagonalRightVictory(index);
            if (isVictory)
            {
                gameState = (currentPlayerType == Opponent.Cross) ? GameState.FinishedVictoryCross : GameState.FinishedVictoryNought;
                Debug.Log(((currentPlayerType == Opponent.Cross) ? "CROSS" : "NOUGHT") + " VICTORY!");
                gameEnd();
                return;
            }

            // check draw:
            if (allCellsFilled())
            {
                gameState = GameState.FinishedDraw;
                Debug.Log("DRAW! =(");
                gameEnd();
                return;
            }

            // next turn:
            currentClickedCell = GameLogic.INVALID_CELL;
            currentPlayerType = (currentPlayerType == Opponent.Cross) ? Opponent.Nought : Opponent.Cross;
            TurnCompleted();
        }
    }
}
