﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ArmorCellBehaviour : MonoBehaviour, IPointerClickHandler
{
    public int row;
    public int col;
    public GameLogic gameLogic;
    public Material crossMaterial;
    public Material noughtMaterial;
    public MeshRenderer innerArmorRenderer;

    public void OnPointerClick(PointerEventData data)
    {
        if (gameLogic.currentClickedCell != GameLogic.INVALID_CELL)
        {
            return;
        }

        gameLogic.currentClickedCell = new GameLogic.CellIndex(row, col);

        Vector3 inputPosition = Input.mousePosition;
        inputPosition.z = 6.35f;    // distance from camera near plane to armor cell plane
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(inputPosition);
        gameLogic.generateBomb(worldPosition);
    }

    void OnTriggerEnter(Collider other)
    {
        if (gameLogic.currentClickedCell.row != row || gameLogic.currentClickedCell.col != col)
        {
            return;
        }

        if (gameLogic.currentPlayerType == GameLogic.Opponent.Cross)
        {
            innerArmorRenderer.material = crossMaterial;
        }
        else if (gameLogic.currentPlayerType == GameLogic.Opponent.Nought)
        {
            innerArmorRenderer.material = noughtMaterial;
        }
        Destroy(this.gameObject, 1);
        gameLogic.processTurn(new GameLogic.CellIndex(row, col));
    }
}
