using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartButtonAnimation : MonoBehaviour
{
    public void Awake()
    {
        Animation animation = GetComponent<Animation>();
        animation.Play("Test");
    }
}
