using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "BrutalTicTacToe/GameSettings")]
public class GameSettings : ScriptableObject
{
    public Color crossBackgroundColor;
    public Color noughBacgroundColor;
}
