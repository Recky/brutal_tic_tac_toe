﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketScript : MonoBehaviour
{
    [SerializeField] private GameObject explode;
    public AudioSource explosionAudio;

    private GameObject tempExplosion;

    void OnTriggerEnter(Collider other)
    {
        tempExplosion = Instantiate(explode, transform);
        explosionAudio.Play(0);
        Destroy(this.gameObject, 1);
    }
}
