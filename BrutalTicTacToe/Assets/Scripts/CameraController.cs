using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Camera playerCamera;

    [SerializeField] private GameLogic gameLogic;
    [SerializeField] private GameSettings gameSettings;

    private void Awake()
    {
        gameLogic.TurnCompleted += GameLogic_TurnCompleted;
        UpdateBackgroundColor();
    }

    private void OnDestroy()
    {
        gameLogic.TurnCompleted -= GameLogic_TurnCompleted;
    }

    private void GameLogic_TurnCompleted()
    {
        UpdateBackgroundColor();
    }

    private void UpdateBackgroundColor()
    {
        this.playerCamera.backgroundColor = (gameLogic.currentPlayerType == GameLogic.Opponent.Cross) ? gameSettings.crossBackgroundColor : gameSettings.noughBacgroundColor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
