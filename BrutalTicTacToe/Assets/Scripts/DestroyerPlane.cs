﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyerPlane : MonoBehaviour
{
    public GameLogic gameLogic;

    void OnTriggerEnter(Collider other)
    {
        // TODO: maybe later do readyInput and processInput game states..
        gameLogic.currentClickedCell = GameLogic.INVALID_CELL;
        Destroy(other.gameObject, 1);
    }
}
